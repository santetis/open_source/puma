import 'package:build_cli_annotations/build_cli_annotations.dart';

part 'version.g.dart';

final versionArgsParser = _$parserForVersionArgs;

@CliOptions()
class VersionArgs {
  @CliOption(
    name: 'breaking',
    defaultsTo: false,
    help:
        'Automatically update the package version when there is a breaking change.',
  )
  final bool breaking;

  final bool breakingWasParsed;

  @CliOption(
    name: 'major',
    defaultsTo: false,
    help: 'Update the major version number of the package.',
  )
  final bool major;

  final bool majorWasParsed;

  @CliOption(
    name: 'minor',
    defaultsTo: false,
    help: 'Update the minor version number of the package.',
  )
  final bool minor;

  final bool minorWasParsed;

  @CliOption(
    name: 'patch',
    defaultsTo: false,
    help: 'Update the patch version number of the package.',
  )
  final bool patch;

  final bool patchWasParsed;

  VersionArgs({
    this.breaking,
    this.breakingWasParsed,
    this.major,
    this.majorWasParsed,
    this.minor,
    this.minorWasParsed,
    this.patch,
    this.patchWasParsed,
  });
}
