import 'package:args/args.dart';
import 'package:puma/src/args/flutter.dart';
import 'package:puma/src/args/global.dart';
import 'package:puma/src/args/package.dart';
import 'package:puma/src/args/version.dart';
import 'package:puma/src/command/flutter.dart';
import 'package:puma/src/command/package.dart';
import 'package:puma/src/command/version.dart';

final parser = initParser();

ArgParser initParser() {
  final rootParser = globalArgsParser;
  rootParser.addCommand(PackageCommand.commandName, packageArgsParser);
  rootParser.addCommand(VersionCommand.commandName, versionArgsParser);
  final flutter = ArgParser();
  flutter.addCommand(FlutterAssetCommand.commandName, flutteAssetsrArgsParser);
  rootParser.addCommand(FlutterCommand.commandName, flutter);
  return rootParser;
}
