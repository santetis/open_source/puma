import 'package:build_cli_annotations/build_cli_annotations.dart';

part 'global.g.dart';

@CliOptions()
class GlobalArgs {
  @CliOption(
    name: 'pub-host-url',
    defaultsTo: 'https://pub.dartlang.org',
    help: 'Define the url of the pub instance.',
    valueHelp: 'pub_host_url',
  )
  final String pubHostUrl;

  GlobalArgs({this.pubHostUrl});
}

final globalArgsParser = _$parserForGlobalArgs;
