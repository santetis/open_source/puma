import 'package:build_cli_annotations/build_cli_annotations.dart';

part 'package.g.dart';

final packageArgsParser = _$parserForPackageArgs;

@CliOptions()
class PackageArgs {
  @CliOption(
    name: 'install',
    abbr: 'i',
    help: 'Add a package to your pubspec.yaml.',
    valueHelp: 'package_name[:[version]]',
  )
  final String install;

  final bool installWasParsed;

  @CliOption(
    name: 'delete',
    abbr: 'd',
    help: 'Delete a package from your pubspec.yaml.',
    valueHelp: 'package_name',
  )
  final String delete;

  final bool deleteWasParsed;

  @CliOption(
    name: 'dev',
    defaultsTo: false,
    help:
        'Put the package addition to the dev_dependencies section of your pubspec.',
  )
  final bool devDependencies;

  PackageArgs({
    this.install,
    this.installWasParsed,
    this.delete,
    this.deleteWasParsed,
    this.devDependencies,
  });
}
