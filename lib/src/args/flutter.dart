import 'package:build_cli_annotations/build_cli_annotations.dart';

part 'flutter.g.dart';

final flutteAssetsrArgsParser = _$parserForFlutteAssetsrArgs;

@CliOptions()
class FlutteAssetsrArgs {
  @CliOption(
    name: 'add',
    abbr: 'a',
    help: 'Add assets to your pubspec.yaml.',
    valueHelp: 'directory_or_file',
  )
  final String add;

  final bool addWasParsed;

  FlutteAssetsrArgs({
    this.add,
    this.addWasParsed,
  });
}
