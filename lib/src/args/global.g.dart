// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'global.dart';

// **************************************************************************
// CliGenerator
// **************************************************************************

GlobalArgs _$parseGlobalArgsResult(ArgResults result) =>
    GlobalArgs(pubHostUrl: result['pub-host-url'] as String);

ArgParser _$populateGlobalArgsParser(ArgParser parser) => parser
  ..addOption('pub-host-url',
      help: 'Define the url of the pub instance',
      valueHelp: 'pub_host_url',
      defaultsTo: 'https://pub.dartlang.org');

final _$parserForGlobalArgs = _$populateGlobalArgsParser(ArgParser());

GlobalArgs parseGlobalArgs(List<String> args) {
  final result = _$parserForGlobalArgs.parse(args);
  return _$parseGlobalArgsResult(result);
}
