// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'flutter.dart';

// **************************************************************************
// CliGenerator
// **************************************************************************

FlutteAssetsrArgs _$parseFlutteAssetsrArgsResult(ArgResults result) =>
    FlutteAssetsrArgs(
        add: result['add'] as String, addWasParsed: result.wasParsed('add'));

ArgParser _$populateFlutteAssetsrArgsParser(ArgParser parser) => parser
  ..addOption('add',
      abbr: 'a',
      help: 'Add assets to your pubspec.yaml.',
      valueHelp: 'directory_or_file');

final _$parserForFlutteAssetsrArgs =
    _$populateFlutteAssetsrArgsParser(ArgParser());

FlutteAssetsrArgs parseFlutteAssetsrArgs(List<String> args) {
  final result = _$parserForFlutteAssetsrArgs.parse(args);
  return _$parseFlutteAssetsrArgsResult(result);
}
