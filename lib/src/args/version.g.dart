// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'version.dart';

// **************************************************************************
// CliGenerator
// **************************************************************************

VersionArgs _$parseVersionArgsResult(ArgResults result) => VersionArgs(
    breaking: result['breaking'] as bool,
    breakingWasParsed: result.wasParsed('breaking'),
    major: result['major'] as bool,
    majorWasParsed: result.wasParsed('major'),
    minor: result['minor'] as bool,
    minorWasParsed: result.wasParsed('minor'),
    patch: result['patch'] as bool,
    patchWasParsed: result.wasParsed('patch'));

ArgParser _$populateVersionArgsParser(ArgParser parser) => parser
  ..addFlag('breaking',
      help:
          'Automatically update the package version when there is a breaking change.',
      defaultsTo: false)
  ..addFlag('major',
      help: 'Update the major version number of the package.',
      defaultsTo: false)
  ..addFlag('minor',
      help: 'Update the minor version number of the package.',
      defaultsTo: false)
  ..addFlag('patch',
      help: 'Update the patch version number of the package.',
      defaultsTo: false);

final _$parserForVersionArgs = _$populateVersionArgsParser(ArgParser());

VersionArgs parseVersionArgs(List<String> args) {
  final result = _$parserForVersionArgs.parse(args);
  return _$parseVersionArgsResult(result);
}
