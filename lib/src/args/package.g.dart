// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'package.dart';

// **************************************************************************
// CliGenerator
// **************************************************************************

PackageArgs _$parsePackageArgsResult(ArgResults result) => PackageArgs(
    install: result['install'] as String,
    installWasParsed: result.wasParsed('install'),
    delete: result['delete'] as String,
    deleteWasParsed: result.wasParsed('delete'),
    devDependencies: result['dev'] as bool);

ArgParser _$populatePackageArgsParser(ArgParser parser) => parser
  ..addOption('install',
      abbr: 'i',
      help: 'Add a package to your pubspec.yaml.',
      valueHelp: 'package_name[:[version]]')
  ..addOption('delete',
      abbr: 'd',
      help: 'Delete a package from your pubspec.yaml.',
      valueHelp: 'package_name')
  ..addFlag('dev',
      help:
          'Put the package addition to the dev_dependencies section of your pubspec.',
      defaultsTo: false);

final _$parserForPackageArgs = _$populatePackageArgsParser(ArgParser());

PackageArgs parsePackageArgs(List<String> args) {
  final result = _$parserForPackageArgs.parse(args);
  return _$parsePackageArgsResult(result);
}
