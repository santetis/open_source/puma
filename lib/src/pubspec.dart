import 'package:meta/meta.dart';
import 'package:pub_semver/pub_semver.dart';
import 'package:puma/src/environment.dart';
import 'package:puma/src/executable.dart';
import 'package:puma/src/flutter.dart';
import 'package:puma/src/package_details.dart';
import 'package:puma/src/utils/map_to_yaml.dart';
import 'package:yaml/yaml.dart';

class Pubspec {
  final String name;
  final Version version;
  final String description;
  final List<String> authors;
  final String homepage;
  final String documentation;
  final List<PackageDetails> dependencies;
  final List<PackageDetails> devDependencies;
  final List<PackageDetails> dependencyOverrides;
  final Environment environment;
  final List<Executable> executables;
  final String publishTo;

  final Flutter flutter;

  const Pubspec({
    @required this.name,
    @required this.version,
    @required this.description,
    @required this.environment,
    this.authors = const <String>[],
    this.homepage,
    this.documentation,
    this.dependencies = const <PackageDetails>[],
    this.devDependencies = const <PackageDetails>[],
    this.dependencyOverrides = const <PackageDetails>[],
    this.executables = const <Executable>[],
    this.publishTo,
    this.flutter = const Flutter(),
  });

  factory Pubspec.fromJson(Map<String, dynamic> json) {
    final name = json['name'];
    final version =
        json.containsKey('version') ? Version.parse(json['version']) : null;
    final description = json['description'];
    final environment = json.containsKey('environment')
        ? Environment.fromJson(json['environment'].cast<String, String>())
        : null;
    final author = json['author'];
    final authors = json['authors'] as List;
    final homepage = json['homepage'];
    final documentation = json['documentation'];
    final dependencies = (json['dependencies'] as Map)
            ?.cast<String, dynamic>()
            ?.entries
            ?.map((entry) => PackageDetails.fromJson({entry.key: entry.value}))
            ?.toList() ??
        [];
    final devDependencies = (json['dev_dependencies'] as Map)
            ?.cast<String, dynamic>()
            ?.entries
            ?.map((entry) => PackageDetails.fromJson({entry.key: entry.value}))
            ?.toList() ??
        [];
    final dependencyOverrides = (json['dependency_overrides'] as Map)
            ?.cast<String, dynamic>()
            ?.entries
            ?.map((entry) => PackageDetails.fromJson({entry.key: entry.value}))
            ?.toList() ??
        [];
    final executables = (json['executables'] as Map)
            ?.cast<String, String>()
            ?.entries
            ?.map((entry) => Executable.fromJson({entry.key: entry.value}))
            ?.toList() ??
        [];
    final flutter = json.containsKey('flutter')
        ? Flutter.fromJson((json['flutter'] as Map).cast<String, dynamic>())
        : Flutter();
    final publishTo = json['publish_to'];
    return Pubspec(
      name: name,
      version: version,
      description: description,
      environment: environment,
      authors: authors?.cast<String>() ?? ((author != null) ? [author] : []),
      homepage: homepage,
      documentation: documentation,
      dependencies: dependencies,
      devDependencies: devDependencies,
      dependencyOverrides: dependencyOverrides,
      executables: executables,
      flutter: flutter,
      publishTo: publishTo,
    );
  }

  factory Pubspec.cloneFromOld(
    Pubspec old, {
    List<PackageDetails> dependencies,
    List<PackageDetails> devDependencies,
    Version version,
    Flutter flutter,
  }) =>
      Pubspec(
        name: old.name,
        version: version ?? old.version,
        description: old.description,
        environment: old.environment,
        authors: old.authors,
        homepage: old.homepage,
        documentation: old.documentation,
        dependencies: dependencies ?? old.dependencies,
        devDependencies: devDependencies ?? old.devDependencies,
        dependencyOverrides: old.dependencyOverrides,
        executables: old.executables,
        flutter: flutter ?? old.flutter,
      );

  bool needPubGet(Pubspec other) =>
      dependencies.length != other.dependencies.length ||
      devDependencies.length != other.devDependencies.length ||
      name != other.name ||
      dependencyOverrides.length != other.dependencyOverrides.length;

  bool versionChanged(Pubspec other) => version != other.version;

  Pubspec nextBreakingVersion() => _changeVersion(version.nextBreaking);

  Pubspec nextMajorVersion() => _changeVersion(version.nextMajor);

  Pubspec nextMinorVersion() => _changeVersion(version.nextMinor);

  Pubspec nextPatchVersion() => _changeVersion(version.nextPatch);

  Pubspec _changeVersion(Version version) =>
      Pubspec.cloneFromOld(this, version: version);

  Pubspec addDependencies(PackageDetails package) => Pubspec.cloneFromOld(
        this,
        dependencies: (Set<PackageDetails>()
              ..addAll(dependencies)
              ..add(package))
            .toList(),
      );

  Pubspec removeDependencies(String packageName) => Pubspec.cloneFromOld(this,
      dependencies: dependencies.where((d) => d.name != packageName).toList());

  Pubspec addDevDependencies(PackageDetails package) => Pubspec.cloneFromOld(
        this,
        devDependencies: (Set<PackageDetails>()
              ..addAll(devDependencies)
              ..add(package))
            .toList(),
      );

  Pubspec addFlutterAsset(String asset) =>
      Pubspec.cloneFromOld(this, flutter: flutter.addAsset(asset));

  Pubspec addFlutterAssets(List<String> assets) =>
      Pubspec.cloneFromOld(this, flutter: flutter.addAssets(assets));

  Pubspec removeDevDependencies(String packageName) =>
      Pubspec.cloneFromOld(this,
          devDependencies:
              devDependencies.where((d) => d.name != packageName).toList());

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = name;
    if (version != null) {
      map['version'] = version.toString();
    }
    if (description != null) {
      map['description'] = description;
    }
    if (environment != null) {
      map['environment'] = environment.toMap();
    }
    if (authors.length == 1) {
      map['author'] = authors.first;
    }
    if (authors.length > 1) {
      map['authors'] = authors;
    }
    if (homepage != null) {
      map['homepage'] = homepage;
    }
    if (documentation != null) {
      map['documentation'] = documentation;
    }
    if (dependencies.isNotEmpty) {
      map['dependencies'] = Map.fromEntries(
          dependencies.map<MapEntry>((d) => d.toMapEntry()).toList());
    }
    if (devDependencies.isNotEmpty) {
      map['dev_dependencies'] = Map.fromEntries(
          devDependencies.map<MapEntry>((d) => d.toMapEntry()).toList());
    }
    if (dependencyOverrides.isNotEmpty) {
      map['dependency_overrides'] = Map.fromEntries(
          dependencyOverrides.map<MapEntry>((d) => d.toMapEntry()).toList());
    }
    if (executables.isNotEmpty) {
      map['executables'] = Map.fromEntries(
          executables.map<MapEntry>((d) => d.toMapEntry()).toList());
    }
    if (flutter?.isNotEmpty == true) {
      map['flutter'] = flutter.toMap();
    }
    return map;
  }

  String toYaml() => yamlToString(YamlMap.wrap(toJson()));
}
