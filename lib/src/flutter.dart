class FontAsset {
  final String asset;
  final String style;
  final int weight;

  const FontAsset({this.asset, this.style, this.weight});

  factory FontAsset.fromJson(Map<String, dynamic> json) {
    return FontAsset(
      asset: json['asset'],
      style: json['style'],
      weight: json['weight'],
    );
  }

  Map<String, dynamic> toMap() {
    final map = <String, dynamic>{};
    map['asset'] = asset;
    if (style != null) {
      map['style'] = style;
    }
    if (weight != null) {
      map['weight'] = weight;
    }
    return map;
  }
}

class Font {
  final String family;
  final List<FontAsset> fonts;

  const Font({
    this.family,
    this.fonts,
  });

  factory Font.fromJson(Map<String, dynamic> json) {
    return Font(
      family: json['family'],
      fonts: (json['fonts'] as List)
          .cast<Map<String, dynamic>>()
          .map((font) => FontAsset.fromJson(font))
          .toList(),
    );
  }

  Map<String, dynamic> toMap() => {}..addEntries(
      [
        MapEntry('family', family),
        MapEntry('fonts', fonts.map((f) => f.toMap()).toList()),
      ],
    );
}

class Flutter {
  final List<String> assets;
  final bool useMaterialDesign;
  final List<Font> fonts;

  const Flutter({
    this.assets = const [],
    this.useMaterialDesign = false,
    this.fonts = const [],
  });

  factory Flutter.fromJson(Map<String, dynamic> json) {
    return Flutter(
        assets: (json['assets'] as List)?.cast<String>() ?? [],
        useMaterialDesign: json['uses-material-design'] == true,
        fonts: (json['fonts'] as List)
            ?.cast<Map>()
            ?.map((font) => Font.fromJson(font))
            ?.toList());
  }

  factory Flutter.cloneFromOld(Flutter old, {List<String> assets}) =>
      Flutter(assets: assets ?? old.assets);

  Flutter addAsset(String asset) => Flutter.cloneFromOld(this,
      assets: (Set<String>()
            ..addAll(assets)
            ..add(asset))
          .toList());

  Flutter addAssets(List<String> assets) => Flutter.cloneFromOld(this,
      assets: (Set<String>()..addAll(this.assets)..addAll(assets)).toList());

  bool get isNotEmpty => assets.isNotEmpty;

  Map<String, dynamic> toMap() {
    final map = <String, dynamic>{};
    if (assets.isNotEmpty) {
      map['assets'] = assets;
    }
    if (useMaterialDesign) {
      map['uses-material-design'] = true;
    }
    if (fonts.isNotEmpty) {
      map['fonts'] = fonts.map((f) => f.toMap()).toList();
    }
    return map;
  }
}
