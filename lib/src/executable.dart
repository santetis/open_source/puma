class Executable {
  final String name;
  final String file;

  const Executable(this.name, this.file);

  factory Executable.fromJson(Map<String, String> json) =>
      Executable(json.keys.first, json.values.first);

  int get hashCode => file.hashCode ^ name.hashCode;

  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Executable && other.file == file && other.name == name);

  MapEntry<String, String> toMapEntry() => MapEntry(name, file);
}
