import 'package:pub_semver/pub_semver.dart';

class Environment {
  final String type;
  final VersionConstraint constraint;

  const Environment(this.type, this.constraint);

  factory Environment.fromJson(Map<String, dynamic> json) {
    if (json.values.first is! String) {
      throw ArgumentError('value must be a String');
    }
    return Environment(
      json.keys.first,
      VersionConstraint.parse(json.values.first),
    );
  }

  int get hashCode => type.hashCode ^ constraint.hashCode;

  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Environment &&
          other.type == type &&
          other.constraint == constraint);

  Map<String, String> toMap() => {type: constraint.toString()};
}
