import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:pub_semver/pub_semver.dart';
import 'package:puma/src/package_details.dart';
import 'package:puma/src/pubspec.dart';
import 'package:pub_semver/pub_semver.dart' as semver;

typedef Future<String> GetLocalPackage(String path);
typedef Future<List<String>> GetAssetsList(String path);

class Puma {
  final String pubHost;
  final Pubspec pubspec;
  final Client client;
  final GetLocalPackage getLocalPackage;
  final GetAssetsList getAssetsList;

  const Puma(
    this.pubHost,
    this.client,
    this.pubspec,
    this.getLocalPackage,
    this.getAssetsList,
  );

  Future<Pubspec> install(String package, {bool dev = false}) async {
    final packageDetails = await _getPackageDetails(package);
    if (!dev) {
      return pubspec.addDependencies(packageDetails);
    }
    return pubspec.addDevDependencies(packageDetails);
  }

  Pubspec delete(String packageName) => pubspec
      .removeDependencies(packageName)
      .removeDevDependencies(packageName);

  Pubspec nextBreakingVersion() => pubspec.nextBreakingVersion();

  Pubspec nextMajorVersion() => pubspec.nextMajorVersion();

  Pubspec nextMinorVersion() => pubspec.nextMinorVersion();

  Pubspec nextPatchVersion() => pubspec.nextPatchVersion();

  Future<Pubspec> addFlutterAsset(String asset) async {
    final assets = await getAssetsList(asset);
    if (assets.length == 1) {
      return pubspec.addFlutterAsset(asset);
    }
    return pubspec.addFlutterAssets(assets);
  }

  Future<PackageDetails> _getPackageDetails(String installInformation) async {
    if (installInformation.contains(':')) {
      final colonIndex = installInformation.indexOf(':');
      final packageName = installInformation.substring(0, colonIndex);
      final v = installInformation.substring(colonIndex + 1);
      if (v.startsWith('git')) {
        //  TODO(kevin): load git resource
        throw 'git is not currently supported';
      } else if (v.isNotEmpty && v != 'latest') {
        return _hostedPackage(packageName,
            version: semver.VersionConstraint.parse(v));
      } else {
        return _hostedPackage(packageName);
      }
    } else {
      return _localOrHostedPackage(installInformation);
    }
  }

  Future<PackageDetails> _localOrHostedPackage(
      String installInformation) async {
    final pathPackage = await _localPackage(installInformation);
    if (pathPackage != null) {
      return pathPackage;
    }
    return _hostedPackage(installInformation);
  }

  Future<PackageDetails> _localPackage(String packagePath) async {
    final normalizePath = packagePath.endsWith(Platform.pathSeparator)
        ? packagePath
        : '$packagePath${Platform.pathSeparator}';
    final name = await getLocalPackage(normalizePath);
    if (name != null) {
      return PathPackage(name, normalizePath);
    }
    return null;
  }

  Future<PackageDetails> _hostedPackage(String packageName,
      {semver.VersionConstraint version}) async {
    if (version == null) {
      return HostedPackage(
        packageName,
        await _getLatestStableVersion(packageName),
      );
    }
    if (await _checkVersionExist(packageName, version)) {
      return HostedPackage(
        packageName,
        Version.parse(version.toString()),
      );
    } else {
      throw ArgumentError(
          'Version ${version.toString()} of package $packageName doesn\'t exist');
    }
  }

  Future<VersionConstraint> _getLatestStableVersion(String packageName) async {
    final response =
        await client.get('$pubHost/api/documentation/$packageName');
    final packageInfos = json.decode(response.body);
    return VersionConstraint.parse('^${packageInfos['latestStableVersion']}');
  }

  Future<bool> _checkVersionExist(
      String packageName, semver.VersionConstraint version) async {
    final response =
        await client.get('$pubHost/api/documentation/$packageName');
    final packageInfos = json.decode(response.body) as Map;
    return (packageInfos['versions'] as List).cast<Map>().firstWhere(
            (d) => version.allows(semver.Version.parse(d['version'])),
            orElse: () => null) !=
        null;
  }
}
