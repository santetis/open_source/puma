import 'package:args/args.dart';
import 'package:puma/src/pubspec.dart';
import 'package:puma/src/puma.dart';

import 'package:puma/src/command/command.dart';

class Commands {
  final Map<String, PumaCommand> commands;
  final PumaCommand helpCommand;

  Commands(List<PumaCommand> commands, this.helpCommand)
      : commands = Map<String, PumaCommand>.fromIterable(commands,
            key: (item) => item.name);

  Future<Pubspec> run(ArgResults results, Puma puma) async {
    if (results.command == null) {
      return await helpCommand.run(results, puma);
    }
    for (final key in commands.keys) {
      if (key == results.command?.name) {
        return await commands[key].run(results.command, puma);
      }
    }
    return null;
  }
}
