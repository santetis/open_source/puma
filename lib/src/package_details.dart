import 'package:pub_semver/pub_semver.dart';

enum PackageType {
  hosted,
  git,
  path,
  sdk,
}

abstract class PackageDetails {
  static PackageType getType(Map<String, dynamic> json) {
    final firstValue = json.values.first;
    if (firstValue is String) {
      return PackageType.hosted;
    } else if (json.values.first is Map) {
      if (firstValue is Map && firstValue.keys.contains('path')) {
        return PackageType.path;
      } else if (firstValue is Map && firstValue.keys.contains('sdk')) {
        return PackageType.sdk;
      } else if (firstValue is Map && firstValue.keys.contains('git')) {
        return PackageType.git;
      }
    }
    return null;
  }

  final String name;

  const PackageDetails(this.name);

  factory PackageDetails.fromJson(Map<String, dynamic> json) {
    final type = getType(json);
    if (type == PackageType.hosted) {
      return HostedPackage.fromJson(json.cast<String, String>());
    } else if (type == PackageType.path) {
      final j = json.map((key, value) => MapEntry<String, Map<String, String>>(
          key, (value as Map).cast<String, String>()));
      return PathPackage.fromJson(j);
    } else if (type == PackageType.sdk) {
      final j = json.map((key, value) => MapEntry<String, Map<String, String>>(
          key, (value as Map).cast<String, String>()));
      return SdkPackage.fromJson(j);
    } else if (type == PackageType.git) {
      final j = json.map(
        (key, value) => MapEntry(
              key,
              (value as Map).cast<String, Map<String, dynamic>>().map(
                    (k, v) => MapEntry(
                          k,
                          v.cast<String, String>(),
                        ),
                  ),
            ),
      );
      return GitPackage.fromJson(j);
    }
    //  TODO: add git dependencies
    throw 'Unknown package type';
  }

  MapEntry<String, dynamic> toMapEntry();
}

class HostedPackage extends PackageDetails {
  final VersionConstraint version;

  const HostedPackage(String name, this.version) : super(name);

  factory HostedPackage.fromJson(Map<String, String> json) => HostedPackage(
      json.keys.first, VersionConstraint.parse(json.values.first));

  int get hashCode => version.hashCode ^ name.hashCode;

  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is HostedPackage &&
          other.version == version &&
          other.name == name);

  MapEntry<String, String> toMapEntry() => MapEntry(name, version.toString());
}

class SdkPackage extends PackageDetails {
  final String sdk;

  const SdkPackage(String name, this.sdk) : super(name);

  factory SdkPackage.fromJson(Map<String, Map<String, String>> json) {
    final name = json.keys.first;
    final sdk = json.values.first['sdk'];
    return SdkPackage(name, sdk);
  }

  int get hashCode => sdk.hashCode ^ name.hashCode;

  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is SdkPackage && other.sdk == sdk && other.name == name);

  MapEntry<String, Map<String, String>> toMapEntry() =>
      MapEntry(name, {'sdk': sdk});
}

class PathPackage extends PackageDetails {
  final String path;

  const PathPackage(String name, this.path) : super(name);

  factory PathPackage.fromJson(Map<String, Map<String, String>> json) {
    final name = json.keys.first;
    final path = json.values.first['path'];
    return PathPackage(name, path);
  }

  int get hashCode => path.hashCode ^ name.hashCode;

  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is PathPackage && other.path == path && other.name == name);

  MapEntry<String, Map<String, String>> toMapEntry() =>
      MapEntry(name, {'path': path});
}

class GitPackage extends PackageDetails {
  final String url;
  final String ref;

  const GitPackage(String name, this.url, this.ref) : super(name);

  factory GitPackage.fromJson(
      Map<String, Map<String, Map<String, String>>> json) {
    final name = json.keys.first;
    print(json.values.first['git']);
    final git = json.values.first['git'];
    final url = git['url'];
    final ref = git['ref'];
    return GitPackage(name, url, ref);
  }

  int get hashCode => url.hashCode ^ ref.hashCode ^ name.hashCode;

  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is GitPackage &&
          other.url == url &&
          other.ref == ref &&
          other.name == name);

  MapEntry<String, Map<String, Map<String, String>>> toMapEntry() {
    final gitMap = <String, String>{};
    gitMap['url'] = url;
    if (ref != null) {
      gitMap['ref'] = ref;
    }
    return MapEntry(
      name,
      {
        'git': gitMap,
      },
    );
  }
}
