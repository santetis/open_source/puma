import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart';
import 'package:puma/src/args/args.dart';
import 'package:puma/src/args/global.dart';
import 'package:puma/src/command/flutter.dart';
import 'package:puma/src/command/help.dart';
import 'package:puma/src/command/package.dart';
import 'package:puma/src/command/version.dart';
import 'package:puma/src/pubspec.dart';
import 'package:puma/src/command/errors.dart';
import 'package:puma/src/commands.dart';
import 'package:puma/src/puma.dart';
import 'package:puma/src/utils/catch_show_usage_exception.dart';
import 'package:puma/src/utils/get_command.dart';
import 'package:puma/src/utils/write_content.dart';
import 'package:yaml/yaml.dart' as yaml;
import 'package:args/args.dart';

/// [PumaCommander] is a class that wrap all the parsing from the args
/// and all the command that puma can run
///
/// This class allow us to specify some field to easily mock the wall project
/// but it's not limited to mock (like running it in the web or in Flutter).
class PumaCommander {
  /// [content] is the content of the current pubspec
  final String content;

  /// [client] define which client has to be used
  ///
  /// this allow us to easily mock the client
  final Client client;

  /// [getLocalPackage] allow you to define a method to get local package based on a path
  final GetLocalPackage getLocalPackage;

  /// [getAssetsList] allow yout to define a method to get the assets based on a path
  final GetAssetsList getAssetsList;

  final _newContentController = StreamController<String>();
  final _printController = StreamController<String>();
  final _needPubGet = StreamController<bool>();

  /// Create a new [PumaCommander]
  PumaCommander(
    this.content,
    this.client,
    this.getLocalPackage,
    this.getAssetsList,
  );

  /// [pubspec] emit event when a new pubspec need to be written to disk
  Stream<String> get pubspec => _newContentController.stream;

  /// [print] emit event that contains something to print
  Stream<String> get print => _printController.stream;

  /// [needPubGet] emit event when you should run pub get
  /// because of the modification done on the pubspec.yaml
  Stream<bool> get needPubGet => _needPubGet.stream;

  /// [run] run the wall project based on the args we pass to it
  Future<void> run(List<String> args) async {
    try {
      final results = parser.parse(args);
      final globalArgs = parseGlobalArgs(args);
      if (args.contains('--pub-host-url')) {
        args..removeRange(args.indexOf('--pub-host-url'), 2);
      }
      final allCommands = [
        PackageCommand(args),
        VersionCommand(args),
        FlutterCommand(args),
        HelpCommand([], args),
      ];
      final helpCommand = HelpCommand(allCommands, args);
      try {
        final yamlRoot = yaml.loadYaml(content) as yaml.YamlMap;
        final oldPubspec = Pubspec.fromJson(
          json.decode(json.encode(yamlRoot)).cast<String, dynamic>(),
        );
        final puma = Puma(
          globalArgs.pubHostUrl,
          client,
          oldPubspec,
          getLocalPackage,
          getAssetsList,
        );
        final commands = Commands(
          allCommands,
          helpCommand,
        );
        final pubspec = await commands.run(results, puma);
        if (pubspec != null) {
          if (pubspec.needPubGet(oldPubspec)) {
            _needPubGet.add(true);
          }
          if (pubspec.versionChanged(oldPubspec)) {
            _printController.add('${oldPubspec.version} -> ${pubspec.version}');
          }
          final pubspecContent = await concatContent(pubspec.toYaml());
          _newContentController.add(pubspecContent);
        }
      } on ShowUsageException catch (e) {
        _printController.add(e.usage);
      } catch (e, s) {
        final sb = StringBuffer();
        sb.writeln('Oops you found a bug in puma !');
        sb.writeln();
        sb.writeln(
            'Please report us the error here : https://gitlab.com/santetis/open_source/puma/issues/new');
        sb.writeln(
            'If you can include this message it will save us lot of time :)');
        sb.writeln();
        sb.writeln(e);
        sb.writeln(s);
        _printController.add(sb.toString());
      }
    } on ArgParserException catch (e) {
      final allCommands = [
        PackageCommand(args),
        VersionCommand(args),
        FlutterCommand(args),
        HelpCommand([], args),
      ];
      final helpCommand = HelpCommand(allCommands, args);
      final command = await getCommand(e.commands, allCommands) ?? helpCommand;
      final help = await catchShowUsageException(() async {
        await command.run(null, null);
      });
      if (help != null) {
        if (e.commands.isNotEmpty) {
          _printController.add(help);
        } else {
          _printController.add('${e.message}\n\n$help');
        }
      }
    } finally {
      _newContentController.close();
      _printController.close();
      _needPubGet.close();
    }
  }
}
