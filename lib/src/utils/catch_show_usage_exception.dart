import 'dart:async';

import 'package:puma/src/command/errors.dart';

Future<String> catchShowUsageException(FutureOr<void> Function() f) async {
  try {
    await f();
  } on ShowUsageException catch (e) {
    return e.usage;
  }
  return null;
}
