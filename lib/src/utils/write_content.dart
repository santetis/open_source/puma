Future<String> concatContent(String yaml) async {
  if (yaml == null) {
    return null;
  }
  final sb = StringBuffer();
  sb.writeln('# GENERATED CODE - DO NOT MODIFY BY HAND');
  sb.writeln('');
  sb.writeln('# Pubspec managed entirely with puma');
  sb.write(yaml);
  return sb.toString();
}
