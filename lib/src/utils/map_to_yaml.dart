import 'dart:convert';

final _unquotableYamlString = RegExp(r"^[a-zA-Z_-][a-zA-Z_0-9-]*$");

String yamlToString(data) {
  var buffer = StringBuffer();

  _stringify(bool isMapValue, String indent, data, bool inArray) {
    // TODO(kevin): Serialize using the YAML library once it supports
    // serialization.

    // Use indentation for (non-empty) maps.
    if (data is Map && data.isNotEmpty) {
      if (isMapValue) {
        buffer.writeln();
        indent += '  ';
      }

      // Sort the keys. This minimizes deltas in diffs.
      var keys = data.keys.toList();
      keys.sort((a, b) => a.toString().compareTo(b.toString()));

      var first = true;
      for (var key in keys) {
        if (!first) buffer.writeln();
        if (first && inArray) {
          buffer.write('$indent- ');
        } else if (inArray) {
          indent += '  ';
          buffer.write('$indent');
        } else {
          buffer.write(indent);
        }
        first = false;

        var keyString = key;
        if (key is! String || !_unquotableYamlString.hasMatch(key)) {
          keyString = jsonEncode(key);
        }

        buffer.write('$keyString:');
        _stringify(true, indent, data[key], data[key] is List);
      }

      return;
    }
    if (data is List && data.isNotEmpty) {
      if (inArray) {
        indent += '  ';
      }
      data.forEach((i) {
        buffer.writeln();
        _stringify(false, indent, i, true);
      });
      return;
    }
    if (data is String && inArray) {
      buffer.write('$indent- $data');
      return;
    }

    // Everything else we just stringify using JSON to handle escapes in
    // strings and number formatting.
    var string = data;

    // Don't quote plain strings if not needed.
    if (data is! String || !_unquotableYamlString.hasMatch(data)) {
      string = jsonEncode(data);
    }

    if (isMapValue) {
      buffer.write(' $string');
    } else {
      buffer.write('$indent$string');
    }
  }

  _stringify(false, '', data, false);
  return buffer.toString();
}
