import 'package:puma/src/command/command.dart';

Future<PumaCommand> getCommand(
    List<String> currentCommands, List<PumaCommand> allCommands) async {
  if (currentCommands == null || currentCommands.isEmpty) {
    return null;
  }
  final command = allCommands.firstWhere((c) => c.name == currentCommands.first,
      orElse: () => null);
  if (currentCommands.length == 1) {
    return command;
  }
  if (command != null) {
    return getCommand(currentCommands.sublist(1), command.children);
  }
  return null;
}
