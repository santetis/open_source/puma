import 'dart:async';

import 'package:args/args.dart';
import 'package:puma/src/args/global.dart';
import 'package:puma/src/command/command.dart';
import 'package:puma/src/command/errors.dart';
import 'package:puma/src/pubspec.dart';
import 'package:puma/src/puma.dart';

class HelpCommand extends PumaCommand {
  static const String commandName = 'help';

  List<PumaCommand> commands;

  HelpCommand(this.commands, List<String> args)
      : super(commandName, 'Display help information for puma.', args);

  @override
  Future<Pubspec> run(ArgResults results, Puma puma) async {
    final sb = StringBuffer();
    sb.writeln('Puma help you to manage your pubspec.yaml.');
    sb.writeln('');
    sb.writeln('Usage: puma <command> [command] [arguments]');
    sb.writeln('');
    sb.writeln('Global options:');
    sb.writeln(globalArgsParser.usage);
    sb.writeln('');
    sb.writeln('Available commands:');
    sb.write(
        commands.map((c) => '${c.name}\t\t\t\t${c.description}').join('\n'));
    throw ShowUsageException(sb.toString());
  }
}
