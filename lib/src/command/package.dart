import 'dart:async';

import 'package:args/args.dart';
import 'package:puma/src/args/package.dart';
import 'package:puma/src/command/command.dart';
import 'package:puma/src/command/errors.dart';
import 'package:puma/src/pubspec.dart';
import 'package:puma/src/puma.dart';

class PackageCommand extends PumaCommand {
  static const String commandName = 'package';

  PackageCommand(List<String> args)
      : super(commandName, 'Manage your project dependencies.', args);

  @override
  Future<Pubspec> run(ArgResults results, Puma puma) async {
    Pubspec newPubspec = null;
    try {
      final packageArgs = parsePackageArgs(args);
      if (packageArgs.installWasParsed) {
        newPubspec = await puma.install(
          packageArgs.install,
          dev: packageArgs.devDependencies,
        );
      } else if (packageArgs.deleteWasParsed) {
        newPubspec = await puma.delete(packageArgs.delete);
      } else {
        throw ShowUsageException(usage());
      }
      return newPubspec;
    } on ArgParserException catch (e) {
      throw ShowUsageException(usage(e.message));
    } catch (e) {
      rethrow;
    }
  }

  @override
  String usage([String leadingError]) {
    final sb = StringBuffer();
    if (leadingError != null) {
      sb.writeln(leadingError);
      sb.writeln('');
    }
    sb.writeln('puma package commands');
    sb.writeln('');
    sb.writeln('Usage: puma package [arguments]');
    sb.write(packageArgsParser.usage);
    return sb.toString();
  }
}
