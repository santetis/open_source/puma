class ShowUsageException implements Exception {
  final String usage;

  const ShowUsageException(this.usage);
}
