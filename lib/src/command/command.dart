import 'dart:async';

import 'package:args/args.dart';
import 'package:puma/src/pubspec.dart';
import 'package:puma/src/puma.dart';

abstract class PumaCommand {
  final String name;
  final String description;
  final List<String> args;
  final List<PumaCommand> children;

  const PumaCommand(this.name, this.description, this.args,
      {this.children = const []});

  FutureOr<Pubspec> run(ArgResults results, Puma puma);

  String usage([String leadingError]) => null;
}
