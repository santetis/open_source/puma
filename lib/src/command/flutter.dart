import 'dart:async';

import 'package:args/args.dart';
import 'package:puma/src/args/flutter.dart';
import 'package:puma/src/command/command.dart';
import 'package:puma/src/command/errors.dart';
import 'package:puma/src/pubspec.dart';
import 'package:puma/src/puma.dart';

class FlutterCommand extends PumaCommand {
  static const String commandName = 'flutter';

  FlutterCommand(List<String> args)
      : super(commandName, 'Manage flutter specific part of the pubspec.', args,
            children: [FlutterAssetCommand(args)]);

  @override
  Future<Pubspec> run(ArgResults results, Puma puma) async {
    for (final child in children) {
      if (results?.command?.name == child.name) {
        return child.run(results, puma);
      }
    }
    throw ShowUsageException(usage());
  }

  @override
  String usage([String leadingError]) {
    final sb = StringBuffer();
    sb.writeln('puma flutter commands');
    sb.writeln('');
    sb.writeln('Usage: puma flutter <subcommands> [arguments]');
    sb.writeln('');
    sb.writeln('Available commands:');
    sb.write(
        children.map((c) => '${c.name}\t\t\t\t${c.description}').join('\n'));
    return sb.toString();
  }
}

class FlutterAssetCommand extends PumaCommand {
  static const String commandName = 'assets';

  FlutterAssetCommand(List<String> args)
      : super(commandName, 'Manage Flutter assets in your pubspec.', args);

  @override
  Future<Pubspec> run(ArgResults results, Puma puma) async {
    try {
      final assetArgs = parseFlutteAssetsrArgs(args);
      if (assetArgs.addWasParsed) {
        return await puma.addFlutterAsset(assetArgs.add);
      }
      throw ShowUsageException(usage());
    } on FormatException catch (e) {
      throw ShowUsageException(usage(e.message));
    } catch (e) {
      rethrow;
    }
  }

  @override
  String usage([String leadingError]) {
    final sb = StringBuffer();
    if (leadingError != null) {
      sb.writeln(leadingError);
      sb.writeln('');
    }
    sb.writeln('puma flutter assets commands');
    sb.writeln('');
    sb.writeln('Usage: puma flutter assets [arguments]');
    sb.write(flutteAssetsrArgsParser.usage);
    return sb.toString();
  }
}
