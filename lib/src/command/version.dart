import 'dart:async';

import 'package:args/args.dart';
import 'package:puma/src/args/version.dart';
import 'package:puma/src/command/command.dart';
import 'package:puma/src/command/errors.dart';
import 'package:puma/src/pubspec.dart';
import 'package:puma/src/puma.dart';

class VersionCommand extends PumaCommand {
  static const String commandName = 'version';

  VersionCommand(List<String> args)
      : super(commandName, 'Manage your project version.', args);

  @override
  Future<Pubspec> run(ArgResults results, Puma puma) async {
    try {
      final versionArgs = parseVersionArgs(args);
      if (versionArgs.breakingWasParsed) {
        return puma.nextBreakingVersion();
      } else if (versionArgs.majorWasParsed) {
        return puma.nextMajorVersion();
      } else if (versionArgs.minorWasParsed) {
        return puma.nextMinorVersion();
      } else if (versionArgs.patchWasParsed) {
        return puma.nextPatchVersion();
      }
      throw ShowUsageException(usage());
    } on FormatException catch (e) {
      throw ShowUsageException(usage(e.message));
    } catch (e) {
      rethrow;
    }
  }

  @override
  String usage([String leadingError]) {
    final sb = StringBuffer();
    if (leadingError != null) {
      sb.writeln(leadingError);
      sb.writeln('');
    }
    sb.writeln('puma version commands');
    sb.writeln('');
    sb.writeln('Usage: puma version [arguments]');
    sb.write(versionArgsParser.usage);
    return sb.toString();
  }
}
