import 'dart:convert';

import 'package:pub_semver/pub_semver.dart';
import 'package:puma/src/environment.dart';
import 'package:puma/src/executable.dart';
import 'package:puma/src/package_details.dart';
import 'package:puma/src/pubspec.dart';
import 'package:test/test.dart';
import 'package:yaml/yaml.dart';

import 'src/pubspec.dart';

void main() {
  group('pubspec', () {
    group('valid parse', () {
      test('all case', () {
        final yaml = loadYaml(allCasePubspecContent) as YamlMap;
        final map = (json.decode(json.encode(yaml)) as Map);
        final pubspec = Pubspec.fromJson(map);
        expect(pubspec.name, 'puma');
        expect(pubspec.publishTo, null);
        expect(pubspec.authors.length, 1);
        expect(pubspec.authors.first, 'Kevin Segaud <segaud.kevin@gmail.com>');
        expect(
            pubspec.homepage, 'https://gitlab.com/santetis/open_source/puma');
        expect(pubspec.description,
            'A simple cli tool to manage your pubspec.yaml');
        expect(
          pubspec.environment,
          Environment(
            'sdk',
            VersionRange(
              min: Version(2, 0, 0),
              max: Version(3, 0, 0),
              includeMin: true,
            ),
          ),
        );
        expect(pubspec.executables.length, 1);
        expect(pubspec.executables.first, Executable('puma', 'main'));
        expect(pubspec.dependencies.length, 3);
        expect(
          pubspec.dependencies.firstWhere((p) => p.name == 'args'),
          HostedPackage('args', VersionConstraint.parse('^1.5.1')),
        );
        expect(
          pubspec.dependencies.firstWhere((p) => p.name == 'test_path'),
          PathPackage('test_path', 'test_path'),
        );
        expect(
          pubspec.dependencies.firstWhere((p) => p.name == 'flutter'),
          SdkPackage('flutter', 'flutter'),
        );
        expect(pubspec.devDependencies.length, 3);
        expect(pubspec.dependencyOverrides.length, 1);
        expect(pubspec.flutter.assets.length, 1);
      });

      test('minimal', () {
        final yaml = loadYaml(minimalPubspecContent) as YamlMap;
        final map = (json.decode(json.encode(yaml)) as Map);
        final pubspec = Pubspec.fromJson(map);
        expect(pubspec.name, 'puma');
        expect(pubspec.authors.length, 0);
        expect(pubspec.description,
            'A simple cli tool to manage your pubspec.yaml');
        expect(
          pubspec.environment,
          Environment(
            'sdk',
            VersionRange(
              min: Version(2, 0, 0),
              max: Version(3, 0, 0),
              includeMin: true,
            ),
          ),
        );
        expect(pubspec.executables.length, 0);
        expect(pubspec.dependencies.length, 0);
        expect(pubspec.devDependencies.length, 0);
        expect(pubspec.dependencyOverrides.length, 0);
      });
    });

    group('toJson()', () {
      test('minimal', () {
        final pubspec = Pubspec(
          name: 'puma',
          version: Version(1, 0, 0),
          environment: Environment(
            'sdk',
            VersionRange(
              min: Version(2, 0, 0),
              max: Version(3, 0, 0),
              includeMin: true,
            ),
          ),
          description: 'Little description',
        );
        final json = pubspec.toJson();
        expect(json.containsKey('name'), isTrue);
        expect(json['name'], 'puma');
        expect(json.containsKey('version'), isTrue);
        expect(json['version'], '1.0.0');
        expect(json.containsKey('description'), isTrue);
        expect(json['description'], 'Little description');
        expect(json.containsKey('environment'), isTrue);
        expect(json['environment'].length, 1);
      });
    });

    group('toYaml()', () {
      test('minimal', () {
        final yaml = loadYaml(minimalPubspecContent) as YamlMap;
        final map = (json.decode(json.encode(yaml)) as Map);
        final pubspec = Pubspec.fromJson(map);
        final y = pubspec.toYaml();
        expect(y, minimalPubspecContent);
      });

      test('all case', () {
        final yaml = loadYaml(allCasePubspecContent) as YamlMap;
        final map = (json.decode(json.encode(yaml)) as Map);
        final pubspec = Pubspec.fromJson(map);
        final y = pubspec.toYaml();
        expect(y, allCasePubspecContent);
      });

      test('test authors', () {
        final minimalWithAuthors = '''
authors:
  - Kevin Segaud
  - Kleak
$minimalPubspecContent''';
        final yaml = loadYaml(minimalWithAuthors) as YamlMap;
        final map = (json.decode(json.encode(yaml)) as Map);
        final pubspec = Pubspec.fromJson(map);
        final y = pubspec.toYaml();
        expect(y, minimalWithAuthors);
      });
    });

    group('version', () {
      test('next breaking', () {
        final yaml = loadYaml(minimalPubspecContent) as YamlMap;
        final map = (json.decode(json.encode(yaml)) as Map);
        var pubspec = Pubspec.fromJson(map);
        pubspec = pubspec.nextBreakingVersion();
        expect(pubspec.version, Version(0, 6, 0));
      });

      test('next major', () {
        final yaml = loadYaml(minimalPubspecContent) as YamlMap;
        final map = (json.decode(json.encode(yaml)) as Map);
        var pubspec = Pubspec.fromJson(map);
        pubspec = pubspec.nextMajorVersion();
        expect(pubspec.version, Version(1, 0, 0));
      });

      test('next minor', () {
        final yaml = loadYaml(minimalPubspecContent) as YamlMap;
        final map = (json.decode(json.encode(yaml)) as Map);
        var pubspec = Pubspec.fromJson(map);
        pubspec = pubspec.nextMinorVersion();
        expect(pubspec.version, Version(0, 6, 0));
      });

      test('next patch', () {
        final yaml = loadYaml(minimalPubspecContent) as YamlMap;
        final map = (json.decode(json.encode(yaml)) as Map);
        var pubspec = Pubspec.fromJson(map);
        pubspec = pubspec.nextPatchVersion();
        expect(pubspec.version, Version(0, 5, 3));
      });
    });

    group('dependencies', () {
      test('add', () {
        final yaml = loadYaml(minimalPubspecContent) as YamlMap;
        final map = (json.decode(json.encode(yaml)) as Map);
        var pubspec = Pubspec.fromJson(map);
        pubspec = pubspec.addDependencies(HostedPackage(
            'new_dependencies', VersionConstraint.parse('1.0.0')));
        expect(pubspec.dependencies.length, 1);
      });

      test('add dev', () {
        final yaml = loadYaml(minimalPubspecContent) as YamlMap;
        final map = (json.decode(json.encode(yaml)) as Map);
        var pubspec = Pubspec.fromJson(map);
        pubspec = pubspec.addDevDependencies(HostedPackage(
            'new_dependencies', VersionConstraint.parse('1.0.0')));
        expect(pubspec.devDependencies.length, 1);
      });

      test('remove', () {
        final yaml = loadYaml(allCasePubspecContent) as YamlMap;
        final map = (json.decode(json.encode(yaml)) as Map);
        var pubspec = Pubspec.fromJson(map);
        pubspec = pubspec.removeDependencies('args');
        expect(pubspec.dependencies.length, 2);
      });

      test('remove dev', () {
        final yaml = loadYaml(allCasePubspecContent) as YamlMap;
        final map = (json.decode(json.encode(yaml)) as Map);
        var pubspec = Pubspec.fromJson(map);
        pubspec = pubspec.removeDevDependencies('test');
        expect(pubspec.devDependencies.length, 2);
      });
    });

    group('flutter', () {
      group('assets', () {
        test('add one', () {
          final yaml = loadYaml(allCasePubspecContent) as YamlMap;
          final map = (json.decode(json.encode(yaml)) as Map);
          var pubspec = Pubspec.fromJson(map);
          pubspec = pubspec.addFlutterAsset('test_asset_1');
          expect(pubspec.flutter.assets.length, 2);
        });

        test('add multiple', () {
          final yaml = loadYaml(allCasePubspecContent) as YamlMap;
          final map = (json.decode(json.encode(yaml)) as Map);
          var pubspec = Pubspec.fromJson(map);
          pubspec = pubspec.addFlutterAssets(['test_asset_2', 'test_asset_3']);
          expect(pubspec.flutter.assets.length, 3);
        });
      });
    });
  });
}
