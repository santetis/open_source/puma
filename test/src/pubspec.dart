final generatedCode = '''# GENERATED CODE - DO NOT MODIFY BY HAND

# Pubspec managed entirely with puma''';

final minimalPubspecContent = '''
description: "A simple cli tool to manage your pubspec.yaml"
environment:
  sdk: ">=2.0.0 <3.0.0"
name: puma
version: "0.5.2"''';

final allCasePubspecContent = '''
author: "Kevin Segaud <segaud.kevin@gmail.com>"
dependencies:
  args: "^1.5.1"
  flutter:
    sdk: flutter
  test_path:
    path: test_path
dependency_overrides:
  test_override: "0.1.0"
description: "A simple cli tool to manage your pubspec.yaml"
dev_dependencies:
  build_cli: "^1.2.1"
  build_runner: "^1.0.0"
  test: "^1.0.0"
documentation: test_doc
environment:
  sdk: ">=2.0.0 <3.0.0"
executables:
  puma: main
flutter:
  assets:
    - test_asset
  fonts:
    - family: Schyler
      fonts:
        - asset: "fonts/Schyler-Regular.ttf"
        - asset: "fonts/Schyler-Italic.ttf"
          style: italic
    - family: "Trajan Pro"
      fonts:
        - asset: "fonts/TrajanPro.ttf"
        - asset: "fonts/TrajanPro_Bold.ttf"
          weight: 700
  uses-material-design: true
homepage: "https://gitlab.com/santetis/open_source/puma"
name: puma
version: "0.5.2"''';
