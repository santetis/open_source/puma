import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:http/testing.dart';

final mockClient = MockClient((request) async {
  if (request.url.path == 'localhost/api/documentation/test_fake_package') {
    return Response(
      json.encode(
        {
          'latestStableVersion': '1.0.0',
          'versions': [
            {'version': '0.1.0'},
            {'version': '1.0.0'},
          ],
        },
      ),
      HttpStatus.ok,
    );
  }
});
