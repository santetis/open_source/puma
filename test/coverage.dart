import 'environment_test.dart' as environment_test;
import 'package_details_test.dart' as package_details_test;
import 'executable_test.dart' as executable_test;
import 'flutter_test.dart' as flutter_test;
import 'pubspec_test.dart' as pubspec_test;
import 'puma_test.dart' as puma_test;
import 'commander_test.dart' as commander_test;

void main() {
  environment_test.main();
  package_details_test.main();
  executable_test.main();
  flutter_test.main();
  pubspec_test.main();
  puma_test.main();
  commander_test.main();
}
