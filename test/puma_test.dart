import 'dart:convert';

import 'package:pub_semver/pub_semver.dart';
import 'package:puma/src/package_details.dart';
import 'package:puma/src/pubspec.dart';
import 'package:puma/src/puma.dart';
import 'package:test/test.dart';
import 'package:yaml/yaml.dart';

import 'src/client_mock.dart';
import 'src/pubspec.dart';
import 'src/package_name_mock.dart';
import 'src/assets_mock.dart';

void main() {
  final yaml = loadYaml(minimalPubspecContent) as YamlMap;
  final map = (json.decode(json.encode(yaml)) as Map);
  final initialPubspec = Pubspec.fromJson(map);
  final pumaHostedWithOneAsset = Puma(
    'localhost',
    mockClient,
    initialPubspec,
    notLocalPackageName,
    getOneAsset,
  );

  final pumaLocalWithMultipleAsset = Puma(
    'localhost',
    mockClient,
    initialPubspec,
    getLocalPackageName,
    getMultipleAsset,
  );

  group('puma', () {
    group('package', () {
      test('add', () async {
        final pubspec =
            await pumaHostedWithOneAsset.install('test_fake_package');
        expect(pubspec.dependencies.length, 1);
      });

      test('add specific version', () async {
        final pubspec =
            await pumaHostedWithOneAsset.install('test_fake_package:1.0.0');
        expect(pubspec.dependencies.length, 1);
      });

      test('add latest version', () async {
        final pubspec =
            await pumaHostedWithOneAsset.install('test_fake_package:latest');
        expect(pubspec.dependencies.length, 1);
      });

      test('add not existing version', () async {
        try {
          await pumaHostedWithOneAsset.install('test_fake_package:2.0.0');
        } catch (e) {
          expect(e is ArgumentError, isTrue);
          if (e is ArgumentError) {
            expect(e.message,
                'Version 2.0.0 of package test_fake_package doesn\'t exist');
          }
        }
      });

      test('add local package', () async {
        final pubspec =
            await pumaLocalWithMultipleAsset.install('test_fake_package');
        expect(pubspec.dependencies.length, 1);
        expect(pubspec.dependencies.first is PathPackage, isTrue);
      });

      test('add dev', () async {
        final pubspec = await pumaHostedWithOneAsset
            .install('test_fake_package', dev: true);
        expect(pubspec.devDependencies.length, 1);
      });

      test('remove', () async {
        final pubspec =
            await pumaHostedWithOneAsset.delete('test_fake_package');
        expect(pubspec.devDependencies.length, 0);
      });
    });

    group('version', () {
      test('breaking', () {
        final p = pumaHostedWithOneAsset.nextBreakingVersion();
        expect(p.version, Version(0, 6, 0));
      });

      test('major', () {
        final p = pumaHostedWithOneAsset.nextMajorVersion();
        expect(p.version, Version(1, 0, 0));
      });

      test('minor', () {
        final p = pumaHostedWithOneAsset.nextMinorVersion();
        expect(p.version, Version(0, 6, 0));
      });

      test('patch', () {
        final p = pumaHostedWithOneAsset.nextPatchVersion();
        expect(p.version, Version(0, 5, 3));
      });
    });

    group('flutter', () {
      group('assets', () {
        test('add one', () async {
          final p = await pumaHostedWithOneAsset.addFlutterAsset('test_asset');
          expect(p.flutter.assets.length, 1);
        });

        test('add multiple', () async {
          final p =
              await pumaLocalWithMultipleAsset.addFlutterAsset('test_asset');
          expect(p.flutter.assets.length, 2);
        });
      });
    });
  });
}
