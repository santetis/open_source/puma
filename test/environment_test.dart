import 'dart:convert';

import 'package:puma/src/environment.dart';
import 'package:test/test.dart';
import 'package:yaml/yaml.dart';
import 'package:pub_semver/pub_semver.dart';

void main() {
  group('environment', () {
    test('valid', () {
      final yamlcontent = 'sdk: \'>=2.0.0 <3.0.0\'';
      final yaml = loadYaml(yamlcontent) as YamlMap;
      final map =
          (json.decode(json.encode(yaml)) as Map).cast<String, String>();
      final environment = Environment.fromJson(map);
      expect(environment.type, 'sdk');
      expect(environment.constraint.toString(), '>=2.0.0 <3.0.0');
    });

    test('invalid', () {
      final yamlcontent = '''sdk:
  toto: \'>=2.0.0 <3.0.0\'''';
      final yaml = loadYaml(yamlcontent) as YamlMap;
      final map =
          (json.decode(json.encode(yaml)) as Map).cast<String, dynamic>();
      try {
        Environment.fromJson(map);
      } catch (e) {
        expect(e is ArgumentError, isTrue);
        if (e is ArgumentError) {
          expect(e.message, 'value must be a String');
        }
      }
    });

    test('toMap()', () {
      final sdkVersionConstraint = '>=2.0.0 <3.0.0';
      final environment =
          Environment('sdk', VersionConstraint.parse(sdkVersionConstraint));
      final mapped = environment.toMap();
      expect(mapped.length == 1, isTrue);
      expect(mapped.containsKey('sdk'), isTrue);
      expect(mapped.containsValue(sdkVersionConstraint), isTrue);
    });

    test('equality', () {
      final a = Environment('sdk', VersionConstraint.empty);
      final b = Environment('sdk', VersionConstraint.empty);
      expect(a.hashCode, b.hashCode);
    });
  });
}
