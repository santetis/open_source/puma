import 'dart:async';

import 'package:puma/src/commander.dart';
import 'package:test/test.dart';

import 'src/client_mock.dart';
import 'src/pubspec.dart';
import 'src/package_name_mock.dart';
import 'src/assets_mock.dart';

void main() {
  PumaCommander createHostMultipleAsset() => PumaCommander(
        allCasePubspecContent,
        mockClient,
        notLocalPackageName,
        getMultipleAsset,
      );

  PumaCommander createMinimalHostMultipleAsset() => PumaCommander(
        minimalPubspecContent,
        mockClient,
        notLocalPackageName,
        getMultipleAsset,
      );

  PumaCommander createMinimalHostOneAsset() => PumaCommander(
        minimalPubspecContent,
        mockClient,
        notLocalPackageName,
        getOneAsset,
      );

  group('commander', () {
    test('no command', () async {
      final commander = createHostMultipleAsset();
      final completer = Completer<String>();
      StreamSubscription<String> subscription;
      subscription = commander.print.listen((help) {
        completer.complete(help);
      }, onDone: () {
        subscription.cancel();
      });
      await commander.run([]);
      final content = await completer.future;
      expect(content, '''Puma help you to manage your pubspec.yaml.

Usage: puma <command> [command] [arguments]

Global options:
--pub-host-url=<pub_host_url>    Define the url of the pub instance
                                 (defaults to "https://pub.dartlang.org")

Available commands:
package				Manage your project dependencies.
version				Manage your project version.
flutter				Manage flutter specific part of the pubspec.
help				Display help information for puma.''');
    });

    test('bad args', () async {
      final commander = createHostMultipleAsset();
      final completer = Completer<String>();
      StreamSubscription<String> subscription;
      subscription = commander.print.listen((help) {
        completer.complete(help);
      }, onDone: () {
        subscription.cancel();
      });
      await commander.run(['--verbose', '--zaza', '1']);
      final content = await completer.future;
      expect(content, '''Could not find an option named "verbose".

Puma help you to manage your pubspec.yaml.

Usage: puma <command> [command] [arguments]

Global options:
--pub-host-url=<pub_host_url>    Define the url of the pub instance
                                 (defaults to "https://pub.dartlang.org")

Available commands:
package				Manage your project dependencies.
version				Manage your project version.
flutter				Manage flutter specific part of the pubspec.
help				Display help information for puma.''');
    });

    test('bad package command', () async {
      final commander = createHostMultipleAsset();
      final completer = Completer<String>();
      StreamSubscription<String> subscription;
      subscription = commander.print.listen((help) {
        completer.complete(help);
      }, onDone: () {
        subscription.cancel();
      });
      await commander.run(['package', '-z', 'toto']);
      final content = await completer.future;
      expect(content, '''Could not find an option or flag "-z".

puma package commands

Usage: puma package [arguments]
-i, --install=<package_name[:[version]]>    Add a package to your pubspec.yaml.
-d, --delete=<package_name>                 Delete a package from your pubspec.yaml.
    --[no-]dev                              Put the package addition to the dev_dependencies section of your pubspec.''');
    });

    group('package', () {
      test('no args', () async {
        final commander = createHostMultipleAsset();
        final completer = Completer<String>();
        StreamSubscription<String> subscription;
        subscription = commander.print.listen((content) {
          completer.complete(content);
        }, onDone: () {
          subscription.cancel();
        });
        await commander.run([
          '--pub-host-url',
          'localhost',
          'package',
        ]);
        final content = await completer.future;
        expect(content, '''puma package commands

Usage: puma package [arguments]
-i, --install=<package_name[:[version]]>    Add a package to your pubspec.yaml.
-d, --delete=<package_name>                 Delete a package from your pubspec.yaml.
    --[no-]dev                              Put the package addition to the dev_dependencies section of your pubspec.''');
      });

      test('add hosted', () async {
        final commander = createHostMultipleAsset();
        final pubspecCompleter = Completer<String>();
        StreamSubscription<String> pubspecSubscription;
        pubspecSubscription = commander.pubspec.listen((content) {
          pubspecCompleter.complete(content);
        }, onDone: () {
          pubspecSubscription.cancel();
        });
        final needPubGetCompleter = Completer<bool>();
        StreamSubscription<bool> needPubGetSubscription;
        needPubGetSubscription = commander.needPubGet.listen((_) {
          needPubGetCompleter.complete(true);
        }, onDone: () {
          needPubGetSubscription.cancel();
        });
        await commander.run([
          '--pub-host-url',
          'localhost',
          'package',
          '-i',
          'test_fake_package'
        ]);
        final content = await pubspecCompleter.future;
        final needPubGet = await needPubGetCompleter.future;
        expectLater(needPubGet, true);
        expect(content, '''$generatedCode
author: "Kevin Segaud <segaud.kevin@gmail.com>"
dependencies:
  args: "^1.5.1"
  flutter:
    sdk: flutter
  test_fake_package: "^1.0.0"
  test_path:
    path: test_path
dependency_overrides:
  test_override: "0.1.0"
description: "A simple cli tool to manage your pubspec.yaml"
dev_dependencies:
  build_cli: "^1.2.1"
  build_runner: "^1.0.0"
  test: "^1.0.0"
documentation: test_doc
environment:
  sdk: ">=2.0.0 <3.0.0"
executables:
  puma: main
flutter:
  assets:
    - test_asset
  fonts:
    - family: Schyler
      fonts:
        - asset: "fonts/Schyler-Regular.ttf"
        - asset: "fonts/Schyler-Italic.ttf"
          style: italic
    - family: "Trajan Pro"
      fonts:
        - asset: "fonts/TrajanPro.ttf"
        - asset: "fonts/TrajanPro_Bold.ttf"
          weight: 700
  uses-material-design: true
homepage: "https://gitlab.com/santetis/open_source/puma"
name: puma
version: "0.5.2"''');
      });

      test('delete', () async {
        final commander = createHostMultipleAsset();
        final completer = Completer<String>();
        StreamSubscription<String> subscription;
        subscription = commander.pubspec.listen((content) {
          completer.complete(content);
        }, onDone: () {
          subscription.cancel();
        });
        await commander.run(['package', '-d', 'args']);
        final content = await completer.future;
        expect(content, '''$generatedCode
author: "Kevin Segaud <segaud.kevin@gmail.com>"
dependencies:
  flutter:
    sdk: flutter
  test_path:
    path: test_path
dependency_overrides:
  test_override: "0.1.0"
description: "A simple cli tool to manage your pubspec.yaml"
dev_dependencies:
  build_cli: "^1.2.1"
  build_runner: "^1.0.0"
  test: "^1.0.0"
documentation: test_doc
environment:
  sdk: ">=2.0.0 <3.0.0"
executables:
  puma: main
flutter:
  assets:
    - test_asset
  fonts:
    - family: Schyler
      fonts:
        - asset: "fonts/Schyler-Regular.ttf"
        - asset: "fonts/Schyler-Italic.ttf"
          style: italic
    - family: "Trajan Pro"
      fonts:
        - asset: "fonts/TrajanPro.ttf"
        - asset: "fonts/TrajanPro_Bold.ttf"
          weight: 700
  uses-material-design: true
homepage: "https://gitlab.com/santetis/open_source/puma"
name: puma
version: "0.5.2"''');
      });
    });

    group('version', () {
      test('no args', () async {
        final commander = createMinimalHostMultipleAsset();
        final completer = Completer<String>();
        StreamSubscription<String> subscription;
        subscription = commander.print.listen((content) {
          completer.complete(content);
        }, onDone: () {
          subscription.cancel();
        });
        await commander.run(['version']);
        final content = await completer.future;
        expect(content, '''puma version commands

Usage: puma version [arguments]
--[no-]breaking    Automatically update the package version when there is a breaking change.
--[no-]major       Update the major version number of the package.
--[no-]minor       Update the minor version number of the package.
--[no-]patch       Update the patch version number of the package.''');
      });

      test('bad args', () async {
        final commander = createMinimalHostMultipleAsset();
        final completer = Completer<String>();
        StreamSubscription<String> subscription;
        subscription = commander.print.listen((content) {
          completer.complete(content);
        }, onDone: () {
          subscription.cancel();
        });
        await commander.run(['version', '-z']);
        final content = await completer.future;
        expect(content, '''Could not find an option or flag "-z".

puma version commands

Usage: puma version [arguments]
--[no-]breaking    Automatically update the package version when there is a breaking change.
--[no-]major       Update the major version number of the package.
--[no-]minor       Update the minor version number of the package.
--[no-]patch       Update the patch version number of the package.''');
      });

      test('breaking', () async {
        final commander = createMinimalHostMultipleAsset();
        final completer = Completer<String>();
        StreamSubscription<String> subscription;
        subscription = commander.pubspec.listen((content) {
          completer.complete(content);
        }, onDone: () {
          subscription.cancel();
        });
        await commander.run(['version', '--breaking']);
        final content = await completer.future;
        expect(content, '''$generatedCode
description: "A simple cli tool to manage your pubspec.yaml"
environment:
  sdk: ">=2.0.0 <3.0.0"
name: puma
version: "0.6.0"''');
      });

      test('major', () async {
        final commander = createMinimalHostMultipleAsset();
        final completer = Completer<String>();
        StreamSubscription<String> subscription;
        subscription = commander.pubspec.listen((content) {
          completer.complete(content);
        }, onDone: () {
          subscription.cancel();
        });
        await commander.run(['version', '--major']);
        final content = await completer.future;
        expect(content, '''$generatedCode
description: "A simple cli tool to manage your pubspec.yaml"
environment:
  sdk: ">=2.0.0 <3.0.0"
name: puma
version: "1.0.0"''');
      });

      test('minor', () async {
        final commander = createMinimalHostMultipleAsset();
        final completer = Completer<String>();
        StreamSubscription<String> subscription;
        subscription = commander.pubspec.listen((content) {
          completer.complete(content);
        }, onDone: () {
          subscription.cancel();
        });
        await commander.run(['version', '--minor']);
        final content = await completer.future;
        expect(content, '''$generatedCode
description: "A simple cli tool to manage your pubspec.yaml"
environment:
  sdk: ">=2.0.0 <3.0.0"
name: puma
version: "0.6.0"''');
      });

      test('patch', () async {
        final commander = createMinimalHostMultipleAsset();
        final completer = Completer<String>();
        StreamSubscription<String> subscription;
        subscription = commander.pubspec.listen((content) {
          completer.complete(content);
        }, onDone: () {
          subscription.cancel();
        });
        await commander.run(['version', '--patch']);
        final content = await completer.future;
        expect(content, '''$generatedCode
description: "A simple cli tool to manage your pubspec.yaml"
environment:
  sdk: ">=2.0.0 <3.0.0"
name: puma
version: "0.5.3"''');
      });
    });

    group('flutter', () {
      test('no args', () async {
        final commander = createMinimalHostOneAsset();
        final completer = Completer<String>();
        StreamSubscription<String> subscription;
        subscription = commander.print.listen((content) {
          completer.complete(content);
        }, onDone: () {
          subscription.cancel();
        });
        await commander.run(['flutter']);
        final content = await completer.future;
        expect(content, '''puma flutter commands

Usage: puma flutter <subcommands> [arguments]

Available commands:
assets				Manage Flutter assets in your pubspec.''');
      });

      group('assets', () {
        test('no args', () async {
          final commander = createMinimalHostOneAsset();
          final completer = Completer<String>();
          StreamSubscription<String> subscription;
          subscription = commander.print.listen((content) {
            completer.complete(content);
          }, onDone: () {
            subscription.cancel();
          });
          await commander.run(['flutter', 'assets']);
          final content = await completer.future;
          expect(content, '''puma flutter assets commands

Usage: puma flutter assets [arguments]
-a, --add=<directory_or_file>    Add assets to your pubspec.yaml.''');
        });

        test('bad args', () async {
          final commander = createMinimalHostOneAsset();
          final completer = Completer<String>();
          StreamSubscription<String> subscription;
          subscription = commander.print.listen((content) {
            completer.complete(content);
          }, onDone: () {
            subscription.cancel();
          });
          await commander.run(['flutter', 'assets', '-z']);
          final content = await completer.future;
          expect(content, '''Could not find an option or flag "-z".

puma flutter assets commands

Usage: puma flutter assets [arguments]
-a, --add=<directory_or_file>    Add assets to your pubspec.yaml.''');
        });

        test('add one', () async {
          final commander = createMinimalHostOneAsset();
          final completer = Completer<String>();
          StreamSubscription<String> subscription;
          commander.print.listen(print);
          subscription = commander.pubspec.listen((content) {
            completer.complete(content);
          }, onDone: () {
            subscription.cancel();
          });
          await commander.run(['flutter', 'assets', '--add', 'new_asset']);
          final content = await completer.future;
          expect(content, '''$generatedCode
description: "A simple cli tool to manage your pubspec.yaml"
environment:
  sdk: ">=2.0.0 <3.0.0"
flutter:
  assets:
    - new_asset
name: puma
version: "0.5.2"''');
        });

        test('add multiple', () async {
          final commander = createMinimalHostMultipleAsset();
          final completer = Completer<String>();
          StreamSubscription<String> subscription;
          commander.print.listen(print);
          subscription = commander.pubspec.listen((content) {
            completer.complete(content);
          }, onDone: () {
            subscription.cancel();
          });
          await commander.run(['flutter', 'assets', '--add', 'new_asset']);
          final content = await completer.future;
          expect(content, '''$generatedCode
description: "A simple cli tool to manage your pubspec.yaml"
environment:
  sdk: ">=2.0.0 <3.0.0"
flutter:
  assets:
    - new_asset
    - new_asset_1
name: puma
version: "0.5.2"''');
        });
      });
    });
  });
}
