import 'dart:convert';

import 'package:puma/src/executable.dart';
import 'package:test/test.dart';
import 'package:yaml/yaml.dart';

void main() {
  group('executable', () {
    test('valid', () {
      final yamlcontent = 'exec: \'main\'';
      final yaml = loadYaml(yamlcontent) as YamlMap;
      final map =
          (json.decode(json.encode(yaml)) as Map).cast<String, String>();
      final environment = Executable.fromJson(map);
      expect(environment.name, 'exec');
      expect(environment.file, 'main');
    });

    test('toMapEntry()', () {
      final executable = Executable('exec', 'main');
      final map = <String, String>{}..addEntries([executable.toMapEntry()]);
      expect(map.containsKey('exec'), isTrue);
      expect(map['exec'], 'main');
    });

    test('equality', () {
      final a = Executable('test', 'main_test');
      final b = Executable('test', 'main_test');
      expect(a.hashCode, b.hashCode);
    });
  });
}
