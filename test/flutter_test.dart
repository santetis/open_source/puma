import 'dart:convert';

import 'package:puma/src/flutter.dart';
import 'package:test/test.dart';
import 'package:yaml/yaml.dart';

void main() {
  group('flutter', () {
    test('parse', () {
      final yamlContent = '''assets:
  - test_asset
''';
      final yaml = loadYaml(yamlContent) as YamlMap;
      final map = (json.decode(json.encode(yaml)) as Map);
      final flutter = Flutter.fromJson(map);
      expect(flutter.assets.length, 1);
      expect(flutter.assets.first, 'test_asset');
    });

    test('toJson()', () {
      final flutter = Flutter(assets: ['test_asset']);
      final map = flutter.toMap();
      expect(map.containsKey('assets'), isTrue);
      expect(map['assets'].length, 1);
    });

    test('add asset', () {
      var flutter = Flutter(assets: ['test_asset']);
      flutter = flutter.addAsset('test_asset_2');
      expect(flutter.assets.length, 2);
    });

    test('add assets', () {
      var flutter = Flutter(assets: ['test_asset']);
      flutter = flutter.addAssets(['test_asset_2', 'test_asset_3']);
      expect(flutter.assets.length, 3);
    });
  });
}
