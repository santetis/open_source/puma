import 'dart:convert';

import 'package:pub_semver/pub_semver.dart';
import 'package:puma/src/package_details.dart';
import 'package:test/test.dart';
import 'package:yaml/yaml.dart';

void main() {
  group('package details', () {
    test('valid hosted', () {
      final yamlcontent = 'rxdart: ^0.19.0';
      final yaml = loadYaml(yamlcontent) as YamlMap;
      final map = (json.decode(json.encode(yaml)) as Map);
      final packageDetails = PackageDetails.fromJson(map);
      expect(packageDetails is HostedPackage, isTrue);
      if (packageDetails is HostedPackage) {
        expect(packageDetails.name, 'rxdart');
        expect(packageDetails.version.toString(), '^0.19.0');
      }
    });

    test('valid path', () {
      final yamlcontent = '''rxdart:
  path: toto''';
      final yaml = loadYaml(yamlcontent) as YamlMap;
      final map = json.decode(json.encode(yaml)) as Map;
      final packageDetails = PackageDetails.fromJson(map);
      expect(packageDetails is PathPackage, isTrue);
      if (packageDetails is PathPackage) {
        expect(packageDetails.name, 'rxdart');
        expect(packageDetails.path, 'toto');
      }
    });

    test('valid sdk', () {
      final yamlcontent = '''flutter:
  sdk: flutter''';
      final yaml = loadYaml(yamlcontent) as YamlMap;
      final map = json.decode(json.encode(yaml)) as Map;
      final packageDetails = PackageDetails.fromJson(map);
      expect(packageDetails is SdkPackage, isTrue);
      if (packageDetails is SdkPackage) {
        expect(packageDetails.name, 'flutter');
        expect(packageDetails.sdk, 'flutter');
      }
    });

    test('HostedPackage.toMapEntry()', () {
      final package = HostedPackage(
        'test',
        VersionRange(
          min: Version(1, 0, 0),
          includeMin: true,
        ),
      );
      final map = {}..addEntries([package.toMapEntry()]);
      expect(map.containsKey('test'), isTrue);
      expect(map['test'], '>=1.0.0');
    });

    test('PathPackage.toMapEntry()', () {
      final package = PathPackage('toto', 'toto');
      final map = <String, Map<String, String>>{}
        ..addEntries([package.toMapEntry()]);
      expect(map.containsKey('toto'), isTrue);
      expect(map['toto'], isMap);
      expect(map['toto'].containsKey('path'), isTrue);
      expect(map['toto']['path'], 'toto');
    });

    test('SdkPackage.toMapEntry()', () {
      final package = SdkPackage('toto', 'toto');
      final map = <String, Map<String, String>>{}
        ..addEntries([package.toMapEntry()]);
      expect(map.containsKey('toto'), isTrue);
      expect(map['toto'], isMap);
      expect(map['toto'].containsKey('sdk'), isTrue);
      expect(map['toto']['sdk'], 'toto');
    });
  });
}
