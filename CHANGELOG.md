#   0.6.1

-   add basic doc

#   0.6.0

-   add lot of test for the common path

#   0.5.2

-   remove debug

##  0.5.1

-   fix parsing local package
-   re-add bump version for major, minor and patch

##  0.5.0

-   refactor all the package
-   see the readme on how to use it due to lot of breaking change

##  0.4.3

-   fix indentation in yaml

##   0.4.2

-   add `add-flutter-asset` to the README
-   update CHANGELOG

##  0.4.1

-   fix usage

##  0.4.0

-   Add `add-flutter-asset` parameters

##  0.3.0

- improve README.md
- add run pub get as a command `puma get`

##  0.1.0

- Initial version, created by Stagehand
