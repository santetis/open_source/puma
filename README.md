[![pipeline status](https://gitlab.com/santetis/open_source/puma/badges/master/pipeline.svg)](https://gitlab.com/santetis/open_source/puma/commits/master)
[![coverage report](https://gitlab.com/santetis/open_source/puma/badges/master/coverage.svg)](https://gitlab.com/santetis/open_source/puma/commits/master)
[![Pub](https://img.shields.io/pub/v/puma.svg)](https://pub.dartlang.org/packages/puma)

A simple cli tool to manage your project.

##  Dependencies management

### Install dependencie

You can easily add package with puma.

```sh
puma package -i mongo_dart
```

Like that puma will automatically get the latest version of the package.

If you want a specific version of you package you specify the version like that

```sh
puma package -i mongo_dart:0.0.1
```

If you need to reference a package locally you can do

```sh 
puma package -i ./mongo_dart
```

#### Add a dependencie to the dev_dependencies

To add a dev dependencie you can add --dev to the command

```sh
puma package -i build_runner:^1.0.0 --dev
```

### Remove dependencie

To remove a dependencie you can do like that

```sh
puma package -d mongo_dart
```

##  Version management

puma help you manage version management.

### Breaking change

If you do a breaking change in your project just run
```sh
puma version --breaking
```

To bump the version of your package.

### Major change

If you just need to bump the major version of your package you can do

```sh
puma version --major
```

### Minor change

If you just need to bump the minor version of your package you can do

```sh
puma version --minor
```

### Patch change

If you just need to bump the patch version of your package you can do

```sh
puma version --patch
```

## Flutter management

### Add asset

You can add Flutter asset by using this command

This will add one file :
```sh
puma flutter assets --add lib/main.dart
```

And this will add the entire folder :
```sh
puma flutter assets --add lib
```