import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:puma/puma.dart';
import 'package:http/io_client.dart';
import 'package:yaml/yaml.dart' as yaml;

Future<void> main(List<String> args) async {
  final pubspecFile = File('pubspec.yaml');
  final pubspecContent = await pubspecFile.readAsString();
  PumaCommander(
    pubspecContent,
    IOClient(),
    getLocalPackageName,
    getAssetsList,
  )
    ..pubspec.listen((content) {
      pubspecFile.writeAsStringSync(content, mode: FileMode.write);
    })
    ..print.listen(print)
    ..needPubGet.listen((_) async {
      await pubGet();
    })
    ..run(args);
}

Future<String> getLocalPackageName(String path) async {
  final pubspecPath = '${path}pubspec.yaml';
  final otherPubspec = File(pubspecPath);
  final pubspecExist = await otherPubspec.exists();
  if (pubspecExist) {
    final otherYaml =
        yaml.loadYaml(await otherPubspec.readAsString()) as yaml.YamlMap;
    return otherYaml['name'];
  }
  return null;
}

Future<List<String>> getAssetsList(String path) async {
  final isDirectory = await FileSystemEntity.isDirectory(path);
  if (isDirectory) {
    final d = Directory(path);
    final paths = d.listSync(recursive: true)
      ..removeWhere((e) => e.statSync().type != FileSystemEntityType.file);
    final newAssets = paths.map((entity) => entity.path).toList();
    newAssets;
  }
  return [path];
}

Future<void> pubGet() async {
  final process = await Process.start('pub', ['get']);
  process.stdout.transform(utf8.decoder).listen(stdout.write);
}
